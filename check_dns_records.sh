#!/bin/bash

BASE_DIR=$(dirname $0)
DOMAIN_LIST="$BASE_DIR/Owned Domains.txt"
CURRENT_NAME="dns_records"
CURRENT_DIR="$BASE_DIR/$CURRENT_NAME"
HISTORICAL_DIR="$BASE_DIR/historical"
CURRENT_TAR="$BASE_DIR/current.tar.gz"
PREV_TAR="$BASE_DIR/prev.tar.gz"

usage() {
    cat <<EOF
NAME
    $(basename $0) - A long term DNS verification tool

USAGE
    $(basename $0) [-v] [-s] [-e receipient@example.com -f me@here.com]

DESCRIPTION

    -v      Verbose
    -s      Skip downloads. Just repeat the latest comparison.
    -e      Email recipient
    -f      From address

EOF
    exit 1
}

get_records() {
    declare -a domain_list="$@"
    for l in ${domain_list[@]}; do
        (
            set -e
            dig +nocmd +nottlid -t SOA +noall +answer "$l"
            dig +nocmd +nottlid -t NS +noall +answer "$l"
            dig +nocmd +nottlid -t A +noall +answer "$l"
            dig +nocmd +nottlid -t MX +noall +answer "$l"
            dig +nocmd +nottlid -t TXT +noall +answer "$l"
        )> "$CURRENT_DIR/$l.txt"
        [[ $? -ne 0 ]] && printf "An error occurred while retrieving records for $l\n" >&2
    done
}

print_summary() {
    declare -a domain_list="$@"
    printf "# DNS Records Checked\n\n"
    for d in ${domain_list[@]}; do
        echo " - $d - SOA is "$(grep -o 'SOA *[^ ]*' "$CURRENT_DIR/$d.txt" | cut -f2)
    done
    printf "\n# Current Records\n\n"
}

clean_filenames() {
    sed -e 's,^.*/,,' -e 's/\.[^.]*$//' -ne '/^[^.]/p'
}

remove_soa_update_time() {
    # The serial number on SOA tends to be touched periodically
    sed -r 's/(SOA\s+\S*\s+\S*\s+)[0-9]* /\1/'
}

ignore_cloudfront_ip_changes() {
    # Cloudfront (AWS lambda) moves IPs around
    sed -r 's/(cloudfront\.net\. IN A\s+)[0-9.]*/\1/'
}

find_diffs() {
    local old_tgz="$1"
    local new_tgz="$2"
    declare -a prev_domains=($(tar tzf "$old_tgz"|clean_filenames))
    declare -a new_domains=($(tar tzf "$new_tgz"|clean_filenames))
    declare -a new_only=($(echo "${prev_domains[@]}" "${prev_domains[@]}" "${new_domains[@]}"|tr ' ' '\n'|sort|uniq -u))
    declare -a prev_only=($(echo "${prev_domains[@]}" "${new_domains[@]}" "${new_domains[@]}"  |tr ' ' '\n'|sort|uniq -u))
    declare -a common_domains=($(echo "${prev_domains[@]}" "${new_domains[@]}"  |tr ' ' '\n'|sort|uniq -d))
    local no_diffs=0

    if [[ ${#prev_only[@]} -ne 0 ]]; then
        for d in ${prev_only[@]}; do
            echo "## Only in previous records - $d"
            echo
            tar xzfO "$old_tgz" "$CURRENT_NAME/$d.txt"|sed 's/^/    /'
        done
        echo
        no_diffs=1
    fi
    if [[ ${#new_only[@]} -ne 0 ]]; then
        for d in ${new_only[@]}; do
            echo "## Only in new records - $d"
            echo
            tar xzfO "$new_tgz" "$CURRENT_NAME/$d.txt"|sed 's/^/    /'
        done
        echo
        no_diffs=1
    fi
    for domain in ${common_domains[@]}; do
        local old=$(tar xzfO "$old_tgz" "$CURRENT_NAME/$domain.txt")
        local new=$(tar xzfO "$new_tgz" "$CURRENT_NAME/$domain.txt")
        local old_for_compare=$(echo "$old"|sort|remove_soa_update_time|ignore_cloudfront_ip_changes)
        local new_for_compare=$(echo "$new"|sort|remove_soa_update_time|ignore_cloudfront_ip_changes)
        if diff -bq <(echo "$old_for_compare") <(echo "$new_for_compare")>/dev/null; then
            if [[ $VERBOSE ]]; then
                echo "## $domain"
                echo
                echo "$new"|sed 's/^/    /'
                echo
            fi
        else
            echo "## $domain HAS CHANGED"
            if [[ $VERBOSE ]]; then
                echo "$new" | sed 's/^/    /'
            fi
            echo
            diff -b <(echo "$old_for_compare") <(echo "$new_for_compare")|sed 's/^/    /'
            echo
            no_diffs=1
        fi
    done
    return $no_diffs
}

collect_records() {
    local dest_dir_path="$1"
    local folder_name="$2"
    local base_dir="$3"
    local dest="${dest_dir_path}/${folder_name}_$(date +%Y-%m-%d).tar.gz"
    tar czf "${dest}" -C  "${base_dir}" "${folder_name}" || return 1
    echo "${dest}"
}

download_and_rollover() {
    declare -a domain_list="$@"
    get_records "${domain_list[@]}"
    new_tar_path=$(collect_records "$HISTORICAL_DIR" "$CURRENT_NAME" "$BASE_DIR") || { echo "Failed to collect records" >&2; exit 1; }
    # Only update current.tar.gz if not already pointing to same file. (Multiple runs on same day overwrites)
    if [[ ! $CURRENT_TAR -ef $new_tar_path ]]; then
        if [[ -e $CURRENT_TAR ]]; then
            [[ ! -L $PREV_TAR ]] && { echo "Unable to overwrite $PREV_TAR as it is not a symbolic link" >&2; exit 2; }
            mv "$CURRENT_TAR" "$PREV_TAR"
        fi
        [[ -e $CURRENT_TAR ]] && { echo "Unable to update $CURRENT_TAR as something already exists" >&2; exit 3; }
        ln -s "$new_tar_path" "$CURRENT_TAR"
    fi
}

while getopts ":vse:f:" opt; do
    case $opt in
        v)  VERBOSE=1;;
        s)  SKIP_DOWNLOAD=1;;
        f)  EMAIL_FROM=${OPTARG}
            [[ -n $EMAIL_FROM ]] || usage
            ;;
        e)  EMAIL_RECIPIENT=${OPTARG}
            [[ -n $EMAIL_RECIPIENT ]] || usage
            ;;
        \?) usage;;
        *)  usage;;
    esac
done

: ${SKIP_DOWNLOAD:=}
: ${VERBOSE:=}
: ${EMAIL_RECIPIENT:=}
: ${EMAIL_FROM:=}

[[ -z "$EMAIL_FROM" && -n "$EMAIL_RECIPIENT" ]] && usage
[[ -n "$EMAIL_FROM" && -z "$EMAIL_RECIPIENT" ]] && usage

process() {
    [[ ! -e "$DOMAIN_LIST" ]] && { echo "Domain list file is missing: $DOMAIN_LIST" >&2; exit 1; }
    declare -a domain_list=($(cat "$DOMAIN_LIST"))

    if [[ ! $SKIP_DOWNLOAD ]]; then
        download_and_rollover ${domain_list[@]}
    fi

    [[ $VERBOSE ]] && print_summary ${domain_list[@]}
    # Do a diff on this time and last time. Report discrepancies.
    if [[ -e "$PREV_TAR" && -e "$CURRENT_TAR" ]]; then
        local diff_result=$(find_diffs "$PREV_TAR" "$CURRENT_TAR")
        if [[ -n $EMAIL_RECIPIENT && $? != 0 ]]; then
            ( print_summary ${domain_list[@]}; echo; echo "$diff_result")|markdown| mail \
                -a "From: $EMAIL_FROM" \
                -a "MIME-Version: 1.0" \
                -a "Content-Type: text/html" \
                -s "DNS Record Differences Detected" \
                "${EMAIL_RECIPIENT}"
        else
            [[ -n $diff_result ]] && echo "$diff_result"
        fi
    else
        echo "Skipping diff as previous or current file is missing" >&2
    fi
}

process
